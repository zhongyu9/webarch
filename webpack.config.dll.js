const webpack = require('webpack');
const path = require('path');
const CleanWebpackPlugin = require('clean-webpack-plugin');         //打包之前清理历史目标文件插件
const Es3ifyPlugin = require('es3ify-webpack-plugin');              //IE8 缺少标识符问题
// const ExtractTextPlugin = require('extract-text-webpack-plugin');   //分离css插件

const packageJSON = require('./package.json');
let vendors = Object.keys(packageJSON.dependencies||[]);
vendors.forEach((v,i)=>{
    if(v === 'babel-runtime'){
        vendors.splice(i,1)
    }
});
const config = {
    entry: {
        //获取所有的入口文件
        vendor: vendors
    },
    output: {
        path: path.join(__dirname, 'src'),
        filename: 'vendors/js/[name].js',
        /**
         * output.library
         * 将会定义为 window.${output.library}
         * 在这次的例子中，将会定义为`window.vendor_library`
         */
        library: '[name]'
    },
    resolve: {
        //设置之后再import或require的时候可以省略后缀
        extensions: ['.js', '.jsx', '.json', '.css', '.png', '.jpg', '.jpeg', '.gif'],
        //搜索目录，可以加快搜索的速度
        modules: [
            path.resolve(__dirname, 'src'),
            path.resolve(__dirname, 'node_modules')
        ],
        //给组件取别名，简化import或require,可以将自己写的组件在引入的时候能够像其他组件一样
        alias: {},
    },
    plugins:[
        new Es3ifyPlugin(),
        new webpack.DefinePlugin({
            'process.env': {
                NODE_ENV: process.env.ENV === 'DEV' ? JSON.stringify('development') : JSON.stringify('production')
            }
        }),
        new webpack.DllPlugin({
            /**
             * path
             * 定义 manifest 文件生成的位置
             * [name]的部分由entry的名字替换
             */
            path: path.join(__dirname, 'src/json', '[name]-manifest.json'),
            /**
             * name
             * dll 输出到那个全局变量上
             * 和 output.library 一样即可。
             */
            name: '[name]',
            context: __dirname,

        }),
        //清理旧文件
        new CleanWebpackPlugin(['./src/vendors/js/*', './src/json/vendor-manifest.json']),
    ]
};
if(process.env.ENV === 'PRO'){
    config.plugins.push(new webpack.optimize.UglifyJsPlugin({
        beautify: false,
        mangle: {
            screw_ie8: false,
            keep_fnames: false
        },
        compress: {
            sequences:10,
            drop_debugger:true,
            conditionals:true,
            booleans:true,
            dead_code:true,
            unused:true,
            warnings:false,
            screw_ie8: false
        },
        output:{
            screw_ie8:false
        },
        comments: false,
    }));
}

module.exports = config;
