
React ES6 书写规范
---
1. `static`开头的类属性，如：
```js
static defaultProps = {

}
static propTypes={

}
```
2. 构造函数
```js
constructor(props){
    //this.state={}
}
```
3. `state`(状态) `state`也可以直接在`constructor`里面写
```js
state = {

}
```
4. `getter/setter` (暂时忽略)
5. 组件生命周期方法
6. `_`开始的私有方法
7. 事件方法,`handle*`
8. `render*`开头的方法,有时候`render()`方法里面会分开到不同的函数去渲染
9. `render()`方法


React 规范具体代码实现
---

<img src="images/react.png"/>

```javascript
class Demo extends React.Component{
    // 默认defaultProps,替代原getDefaultProps方法, 注意前面有static
    //或者在最后面 Demo.defaultProps={}
    static defaultProps={
        loading:false
    }
    //替代原propTypes 属性,注意前面有static,属于静态方法.
    //最新版React 需要注意
    //npm install --save-dev prop-types
    //import PropTypes from 'prop-types';
    //第二种写法在类的最后 Demo.propTypes={}
    static propTypes={
        autoPlay:PropTypes.bool.isRequired
    }
    //构造方法
    constructor(props){
        super(props)
        //state初始化
        //this.state={}
    }

    // 初始化state,替代原getInitialState, 注意前面没有static
    state={
        showMenu:false
    }

    //挂在阶段
    //在渲染之前执行 render()方法之前
    componentWillMount=()=>{

    }
    //渲染之后执行 render()方法之后
    componentDidMount=()=>{

    }

    //挂在完成后componentWillMount和componentDidMount不会再执行

    //更新阶段update

    //当props变化的时候执行
    componentWillReceiveProps =(nextProps)=>{}

    //返回true会渲染，false不会渲染，常做性能优化点
    //this.forceUpdate() 强制更新，与shouldComponentUpdate返回true类似
    shouldComponentUpdate=(nextProps,nextState)=>{
        return true;
    }

    //重新渲染之前调用
    componentWillUpdate=(nextProps,nextState)=>{}

    //重新渲染之后
    componentDidUpdate=(prevProps,prevState)=>{}

    //组件卸载之前调用
    componentWillUnmount=()=>{}

    //组件卸载之后调用,常用于清理
    componentDidMount=()=>{}

    //其他的普通私有方法

    // 事件的写法,这里要使用箭头函数,箭头函数不会改变this的指向,否则函数内,this指的就不是当前对象了
    // React.CreatClass方式React会自动绑定this,ES6写法不会.详见下一小节说明.
    //第二种在调用的时候使用this.handleClick.bind(this)
    handleClick=(e)=>{
        this.setState({});//这里的this还是Demo
    }

    //事件命名handleClick=()=>{}



    renderUserList = () => {} // 渲染用户列表

    renderJobOptions = () => {} // 渲染工作下拉option

    render(){
        return (
            <div>Demo</div>
        )
    }
}

export default Demo
```
