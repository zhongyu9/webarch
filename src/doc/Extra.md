[TOC]
# webpack uglify plugin
压缩插件
>yarn add uglifyjs-webpack-plugin --dev

安装依赖
ES6
>yarn add git://github.com/mishoo/UglifyJS2#harmony --dev

ES5
>yarn add uglify-js --dev

# fetch
API:https://developer.mozilla.org/en-US/docs/Web/API/Fetch_API
https://github.com/github/fetch
FetchFirefox 39 以上，和 Chrome 42 以上都被支持了
还可以用 Fetch Polyfil，用于支持那些还未支持 Fetch 的浏览器

>yarn add isomorphic-fetch es6-promise
>yarn add whatwg-fetch es6-promise

```js
fetch(url, {
  credentials: 'include'
})
//credentials: 'include'  可以携带cookie
require('es6-promise').polyfill();
require('isomorphic-fetch');
```


# react router 404问题
https://www.thinktxt.com/react/2017/02/26/react-router-browserHistory-refresh-404-solution.html
` --history-api-fallback`在开发阶段处理`react-router`404问题
或者在devServer 中添加
```js
devServer:{
    historyApiAallback:true
}
```


# Antd：
>yarn add --dev babel-plugin-import

```json
"plugins": [
     ["import",{"libraryName":"antd","style":"css"}]
]
```

# 启动日志
启动的时候添加  --profile > stats.log可以将启动的信息打印在starts.log文件中

# webpack 设置环境变量
> yarn add --dev cross-env
> cross-env BUILD_DEV=true

_兼容不同的平台_

# 设置环境变量
> cross-env BUILD_PRERELEASE=true cross-env CTX=/webpack-demo/

> set BUILD_PRERELEASE=true && set CTX=/webpack-demo/
> export BUILD_PRERELEASE=true && export CTX=/webpack-demo/


# 兼容IE8
> yarn add es5-shim console-polyfill fetch-ie8 babel-polyfill  es3ify-loader

# deep-equal
> yarn add deep-equal
