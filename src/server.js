
var open = require('open');
var express = require('express');
var app = express();

app.use(express.static(__dirname));

var server = app.listen(3000,()=>{
    var host = server.address().address;
    host = host==='::'?'127.0.0.1':host;
    var port = server.address().port;

    console.log('请使用IE浏览器打开 http://%s:%s',host,port);
    //打开IE浏览器
    open(`http://${host}:${port}`,'iexplore');
});
