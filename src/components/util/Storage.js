



class Cookie{
    /**
     * [addCookie 添加cookie]
     * @param {[type]} name   [cookie的名称]
     * @param {[type]} value  [cookie的值]
     * @param {[type]} maxAge [cookie的存活事件，单位是分钟]
     */
    setCookie (name, value, maxAge){
      if (maxAge) {
        let date = new Date();
        date.setTime(date.getTime() + (maxAge * 60 * 1000));
        var expires = ';expires=' + date.toUTCString();
      } else {
        var expires = '';
      }

      document.cookie = name + "=" + encodeURIComponent(value) + expires + "; path=/";
    }
    /**
     * [getCookie 获取cookie的值]
     * @param  {[type]} name [cookie的名称]
     * @return {[type]}      [cookie的值]
     */
    getCookie(name){
        let _name = name.trim() + '=';
        const cookies = document.cookie.split(';');
        let val = '';
        cookies.forEach((cookie,index)=>{
            cookie = cookie.trim();
            if(cookie.indexOf(_name) === 0){
                val = decodeURIComponent(cookie.substring(_name.length,cookie.length));
                return true;
            }
        })

        return val;
    }
    /**
     * [removeCookie 删除cookie]
     * @param  {[type]} name [cookie的名称]
     */
    removeCookie(name){
        setCookie(name,'',-1);
    }
}

export const toJSON=(str)=>{
    return JSON.parse(str);
}
export const toStr=(obj)=>{
    return JSON.stringify(obj);
}

class Storage{
    setItem(k,v){
        localStorage.setItem(k,v);
    }
    getItem(k){
        return localStorage.getItem(k);
    }
    removeItem(k){
        localStorage.removeItem(k);
    }
    clear(){
        localStorage.clear();
    }
}

class TempStorage{
    setItem(k,v){
        sessionStorage.setItem(k,v);
    }
    getItem(k){
        return sessionStorage.getItem(k);
    }
    removeItem(k){
        sessionStorage.removeItem(k);
    }
    clear(){
        sessionStorage.clear();
    }
}
const _Cookie = new Cookie();
const _Storage = new Storage();
const _TempStorage = new TempStorage();
const Store = {}
Store.Cookie = _Cookie;
Store.Storage = _Storage;
Store.TempStorage = _TempStorage;
export default Store
export {
    _Cookie as Cookie,
    _Storage as Storage,
    _TempStorage as TempStorage
}
