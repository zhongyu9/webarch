import React,{Component} from 'react';
import PropTypes from 'prop-types';
let tool={
    isAuth:(auth)=>true
}
let WrapAuth=(WrappedComponent,options)=>class AuthComponent extends Component{
    constructor(props){
        super(props);
    }
    static propTypes = {
        auth:PropTypes.string.isRequired
    }
    render(){
        if(tool.isAuth(this.props.auth)){
            return <WrappedComponent {...this.props}/>
        }else{
            return null
        }
    }
}
