[TOC]
<h1><center>项目说明</center></h1>
# 初始化项目
`yarn install | npm install`

## 1. 开发阶段
> 为了在开发阶段加快编译速度，请优先运行此命令。只需要在合适的时机运行比如第一次运行项目的或者安装了其他依赖的时候运行即可。开发的时候此命令不是必须要运行的
`yarn dll | npm run dll`

> 启动或重启项目,开发阶段此命令是必须的。正常情况下，修改代码后是不需要重启，此架构将会自动编译和刷新。
`yarn dev | npm run dev`

## 2. 发布生产
> 只需要执行一条命令，完成后将dist目录发布即可
`yarn pro | npm run pro`

## 3. 测试IE浏览器的兼容
> 目前的语法和IE浏览器特别是IE8浏览器的兼容有很大问题，所以在发布之前需要进行IE兼容测试
`yarn test | npm run test`
然后打开浏览器，地址栏输入http://ip:port ,按F12查看

# 项目目录结构说明
## dist
> 打包之后的文件
## src
> 源码目录
### assets
> 静态文件
### components
> 自己写的一些公共组件
### doc
> 项目需要的文档
### json
> 项目中用到的json文件
#### alias.json
> 为组件起别名，简化在项目中调用,格式如下。调用的时候直接`import DateUtil`即可
```json
{
    "css": "./src/assets/css",
    "images": "./src/assets/images",
    "module": "./src/module",
    "component": "./src/components",
    "DateUtil": "./src/components/util/DateUtil",
    "FetchAjax": "./src/components/util/FetchAjax",
    "Storage": "./src/components/util/Storage"
}
```
#### assets.json
> 项目中需要的其它依赖信息，比如:`font-awesome`,是一个数组
```json
[
    {
        "filepath":"./src/vendors/font-awesome/css/font-awesome.min.css",
        "includeSourcemap":false,
        "typeOfAsset":"css",
        "publicPath":"vendors/font-awesome/css",
        "outputPath":"/vendors/font-awesome/css"
    }
]
```
#### vendor-manifest.json
> 勿动
#### html-desc.json
> html描述文件，key是入口文件的文件名
```js
{
    "index":{
        "title":"首页",
        "id":"index",       //默认和入口文件名一致
        "filename":"index", //html文件名，默认和入口文件名一致
        "metas":"",         //其他描述信息
    }
}
```
### module
> 各个模块的代码
### page
> 多页面的时候每个页面对应的入口文件(`js`)
### vendors
> 第三方库
### view-template
> 多页面对应的`html`模板文件
## `server.js`
> 为测试环境提供的后台服务，勿动

其他文件勿动
